name := "Kafka Streams"
scalaVersion := "2.12.15"
scalacOptions ++= Seq("-language:implicitConversions", "-deprecation")
libraryDependencies ++= Seq(
  "org.json4s" %% "json4s-jackson" % "4.0.4",
  "com.fasterxml.jackson.core" % "jackson-core" % "2.10.3",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.4",
  "org.scala-lang.modules" %% "scala-xml" % "1.2.0",
  "org.apache.kafka" %% "kafka-streams-scala"% "2.8.1",
  "javax.ws.rs" % "javax.ws.rs-api" % "2.1" artifacts( Artifact("javax.ws.rs-api", "jar", "jar"))
  )
resolvers++= Seq(
  Classpaths.typesafeReleases,
  "confluent" at "https://packages.confluent.io/maven/"
)
