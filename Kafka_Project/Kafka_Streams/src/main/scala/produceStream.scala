

import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import scala.io.Source
import scala.io.StdIn._
import scala.xml._
import java.util.Properties
import com.typesafe.scalalogging.Logger
import scala.util.Random
import com.fasterxml.jackson.databind.node.JsonNodeFactory
import com.fasterxml.jackson.databind.node.ObjectNode



object ProduceStream  extends App {

        
        
        val props:Properties = new Properties()
        props.put("bootstrap.servers", "localhost:9092")
        props.put("schema.registry.url", "http://localhost:8081")
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")
        props.put("acks", "1")

        val producer = new KafkaProducer[String, String](props)
         
        
        println("Enter your topic name :")
        val topic : String= readLine()
        
        
                val xmldata = XML.loadFile("src/main/resources/liste_noms_age.xml")
                val personNodes = (xmldata \\ "person").zipWithIndex
                
            while (true)
                {
                 personNodes.foreach{ case (n, i) =>
                 val key=i.toString()

                 val name  = (n \ "name").text
                 val age = (n \ "age").text.toInt
                 val pers:ObjectNode = JsonNodeFactory.instance.objectNode()

                 pers.put("name",name)
                 pers.put("age",age)
                 
                 val record = new ProducerRecord(topic, key, pers.toString())
                 val metadata = producer.send(record)
                  println(s"key=${record.key()}  value=${record.value()}  \n")
                  Thread.sleep(1000+Random.nextInt(1000))
                  Runtime.getRuntime.addShutdownHook(new Thread(() => producer.close()))
               }
              
            }
            
     
         


  
}
