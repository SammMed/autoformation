

import java.util.{Collections, Properties}
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.JsonNodeFactory
import com.fasterxml.jackson.databind.node.ObjectNode
import java.util.concurrent.TimeUnit
import org.apache.kafka.common.serialization._
import org.apache.kafka.streams.scala.StreamsBuilder
import org.apache.kafka.streams.scala.kstream.{KGroupedStream, KStream, KTable,Grouped,Materialized}
import org.apache.kafka.streams.kstream.Serialized
import org.apache.kafka.streams.{KafkaStreams, StreamsConfig}
import org.apache.kafka.streams._
import org.apache.kafka.connect.json.JsonDeserializer
import org.apache.kafka.connect.json.JsonSerializer
import org.apache.kafka.streams.kstream.Produced




object StreamProcess extends App {
   
 
        
  import org.apache.kafka.streams.scala.Serdes._
  import org.apache.kafka.streams.scala.ImplicitConversions._

        val props:Properties = new Properties()
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "StreamApp")
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092")
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String.getClass.getName)
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG,Serdes.String.getClass.getName)
       
       
        implicit  val jsonSerde:Serde[JsonNode] = Serdes.serdeFrom(new JsonSerializer(),new JsonDeserializer())
        //val longSerde: Serde[Long] = Serdes.Long().asInstanceOf[Serde[Long]]
        val builder = new StreamsBuilder()
    
        val personsStream: KStream[String,JsonNode] = builder.stream[String, JsonNode]("inputTopic")

       
        
        
       
        val outputStream:KStream[String,String]=personsStream.mapValues(
                { v => 
                 
                 val age = v.get("age").asInt()
                 val ageX2:ObjectNode = JsonNodeFactory.instance.objectNode()

                 ageX2.put("ageX2",age*2)
                 
                 
                 (ageX2.toString())
                 
                })
        val outputStream2 :KStream[Integer,String]=personsStream.map(
                {
                        (k,v) =>  
                               
                                val age = v.get("age").asInt()
                                age+1
                            
                }
        )
        val outputStream3:KStream[Integer,String]=personsStream.map(
                {
                        (k,v) =>  
                                val name= v.get("name").asText()
                                val age = v.get("age").asInt()
                                val ageX2:ObjectNode = JsonNodeFactory.instance.objectNode()

                                ageX2.put("ageX2",age*2)
                                (age+1,ageX2.toString())
                            
                }
        )
        
        // val newStream:KTable[String,Long]=personsStream.map((k,v) => ("max",v.get("age").asInstanceOf[Long]))
        // .groupByKey(Grouped.`with`(Serdes.String,longSerde))
        // .reduce(_.max(_))
        

        // newStream.toStream.to("maxAge")(Produced.`with`(Serdes.String(),longSerde))
        
        


        outputStream.to("ageX2")
        outputStream2.to("kagePlus1")(Produced.`with`(Serdes.Integer(),Serdes.String()))
        outputStream3.to("Kageplus1VageX2")(Produced.`with`(Serdes.Integer(),Serdes.String()))
        
        val Stream : KafkaStreams = new KafkaStreams(builder.build(),props)

         Stream.cleanUp()
         Stream.start()
        
        
        
}