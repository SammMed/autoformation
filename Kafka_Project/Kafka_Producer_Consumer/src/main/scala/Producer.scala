import java.util.Properties
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import scala.xml._
import scala.io.Source
import scala.util.control.Breaks.{break, breakable}
import scala.io.StdIn._

object Producer extends App {
     println("\n" +" Enter your topic name :")
    val topic : String= readLine()
    val props:Properties = new Properties()
    props.put("bootstrap.servers","localhost:9092")
    props.put("key.serializer","org.apache.kafka.common.serialization.StringSerializer")
    props.put("value.serializer","org.apache.kafka.common.serialization.StringSerializer")
    props.put("acks","all")

  val producer = new KafkaProducer[String, String](props)
  

  try {
    val xmldata = XML.loadFile("src/main/scala/liste_noms_age.xml")
    val personNodes = (xmldata \\ "person")

    personNodes.foreach{ n =>
    
    val name  = (n \ "name").text
    val age = (n \ "age").text.toInt
    val record = new ProducerRecord[String, String](topic, name, s"Age: $age")
    val metadata = producer.send(record)
    //println(s"sent record(key=${record.key()}  value=${record.value()})"+
     //s"meta(partition= ${metadata.get().partition()}, offset= ${metadata.get().offset()} \n")
    }
     
  }catch{
    
    case e:Exception => e.printStackTrace()
  }finally {
    producer.close()
  }

}