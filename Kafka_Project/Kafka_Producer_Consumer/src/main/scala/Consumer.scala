
import java.util.{Arrays, Properties}
import org.apache.kafka.clients.consumer.KafkaConsumer


object Consumer extends App {

  val props:Properties = new Properties()
  props.put("group.id", "test")
  props.put("bootstrap.servers","localhost:9092")
  props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer") 
  props.put("value.deserializer","org.apache.kafka.common.serialization.StringDeserializer")


  val consumer = new KafkaConsumer(props)
  consumer.subscribe(Arrays.asList("test"))
  try {
    
    while (true) {
      val records = consumer.poll(20)
      records.forEach(record => {
        println("received message :" + record.value )
      })
    }
  }catch{
    case e:Exception => e.printStackTrace()
  }finally {
    consumer.close()
  }
}