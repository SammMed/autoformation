# create a topic
kafka-topics.sh --create --zookeeper zookeeper:2181 --replication-factor 1 --partitions 1 --topic test

# list all topics
kafka-topics.sh --list --zookeeper zookeeper:2181

# liste des commandes utiles pour Docker

# pour créer un Topic 

./bin/kafka-topics.sh --create --zookeeper zookeeper:2181 --replication-factor 1
 --partitions 1 --topic first

#pour lister les topics

./bin/kafka-topics.sh --list --zookeeper zookeeper:2181

# produire un message

./bin/kafka-console-producer.sh --broker-list localhost:9092 --topic test

#consommer un message

kafka-console-consumer --bootstrap-server localhost:9092 --from-beginning --property print.key=true  --property key.separator=" : " --topic ageX2

kafka-console-consumer --topic kagePlus1 --bootstrap-server localhost:9092 \
 --from-beginning \
 --property print.key=true \
 --property key.separator=" : " \
 --key-deserializer "org.apache.kafka.common.serialization.IntegerDeserializer"

kafka-console-consumer --topic Kageplus1VageX2 --bootstrap-server localhost:9092 \
 --from-beginning \
 --property print.key=true \
 --property key.separator=" : " \
 --key-deserializer "org.apache.kafka.common.serialization.IntegerDeserializer"

kafka-console-consumer --topic maxAge --bootstrap-server localhost:9092 \
 --from-beginning \
 --property print.key=true \
 --property key.separator=" : " \
 --value-deserializer "org.apache.kafka.common.serialization.IntegerDeserializer"

#consommer avro format

kafka-avro-console-consumer --bootstrap-server localhost:9092  --value-deserializer io.confluent.kafka.serializers.KafkaAvroDeserializer --topic test  

#In schema registry 
kafka-avro-console-consumer --topic test <--bootstrap-server broker:29092> --from-beginning

kafka-avro-console-consumer \
 --bootstrap-server broker:29092 \
 --from-beginning \
 --topic test  \
 --property schema.registry.url=http://localhost:8081 \
 --property value.schema.id=21\
 --value-deserializer io.confluent.kafka.serializers.KafkaAvroDeserializer

#data retain in topic
kafka-configs --alter --zookeeper zookeeper:2181 --entity-type topics --entity-name test --add-config retention.ms=20000


# Pour arreter docker

docker-compose stop

# exécuter le conteneur Kafka

docker exec -it kafka /bin/sh
# delete topic
kafka-topics.sh --zookeeper zookeeper:2181 --delete  --topic <topic_name>
rmr /brokers/topics/<topic_name>
rmr /admin/delete_topics/<topic_name>