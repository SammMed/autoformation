import org.apache.avro.Schema.Parser
import org.apache.avro.Schema
import org.apache.avro.generic.GenericData
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import scala.io.Source
import scala.io.StdIn._
import scala.xml._
import java.util.Properties
import com.typesafe.scalalogging.Logger



object ProducerAvro  extends App {

        
        
        val props:Properties = new Properties()
        props.put("bootstrap.servers", "localhost:9092")
        props.put("schema.registry.url", "http://localhost:8081")
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
        props.put("value.serializer", "io.confluent.kafka.serializers.KafkaAvroSerializer")
        props.put("acks", "1")

        val producer = new KafkaProducer[String, GenericData.Record](props)
         
        //read avro schema file
        val schemaString = Source.fromURL(getClass.getResource("person.avsc")).mkString
        // Initialize schema
        val schema: Schema = new Schema.Parser().parse(schemaString)
        
        println("Enter your topic name :")
        val topic : String= readLine()
        
        try {
                val xmldata = XML.loadFile("src/main/resources/liste_noms_age.xml")
                val personNodes = (xmldata \\ "person").zipWithIndex
                

                personNodes.foreach{ case (n, i) =>
    
                 val name  = (n \ "name").text
                 val age = (n \ "age").text.toInt
                 val avroRecord = new GenericData.Record(schema)
                 avroRecord.put("name", name)
                 avroRecord.put("age", age)
                 val key=i.toString()
                 val record = new ProducerRecord[String, GenericData.Record](topic, key, avroRecord)
                 val metadata = producer.send(record)
                 println(s"key=${record.key()}  value=${record.value()} partition= ${metadata.get().partition()}, offset= ${metadata.get().offset()} \n")
               }
     
         }catch{
                case e:Exception => e.printStackTrace()
        }finally {
                 producer.close()
         }



  
}
