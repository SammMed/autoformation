import java.util.{Arrays, Properties}
import org.apache.kafka.clients.consumer.KafkaConsumer



object ConsumerAvro extends App {

        val props:Properties = new Properties()
        props.put("group.id", "g1")
        props.put("bootstrap.servers", "localhost:9092")
        props.put("schema.registry.url", "http://localhost:8081")
        props.put("key.deserializer","org.apache.kafka.common.serialization.StringDeserializer")
        props.put("value.deserializer", "io.confluent.kafka.serializers.KafkaAvroDeserializer")
       

        val consumer = new KafkaConsumer(props)
        

        consumer.subscribe(Arrays.asList("test"))
        while(true) {
            //poll for new messages every 20 millis
            val records = consumer.poll(20)

            //print each received record
            records.forEach(record => println("received message(" + record.value +")"))
        }

    //define a listener on a different thread that will close the consumer when the user presses ctrl-c
    Runtime.getRuntime.addShutdownHook(new Thread(() => consumer.close()))
}
