#!/bin/bash
if [ $# -lt 1 ]; then
 echo "enter the path to your avro schema file"
else 
    jq '. | {schema: tojson}' $1  | \
    curl -X POST http://localhost:8081/subjects/test-value/versions \
         -H "application/vnd.schemaregistry.v1+json" \
         -d @-
    
fi