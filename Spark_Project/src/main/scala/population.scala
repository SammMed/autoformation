import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{SparkSession,DataFrame,Row}
import org.apache.spark.sql.types._
import org.apache.spark.sql.Row
import scala.io.StdIn._
import scala.util.control.Breaks.{break, breakable}



 

object Population {
      case class wordpopulation(countryName: String, populationPeryear: List[Long]){
            override def toString() : String = {
          
                  return "Country Name: " + countryName + ",[Population List = " + populationPeryear+"]";
            }
      }
      def main(args: Array[String]): Unit = {
    
            val spark = SparkSession.builder()
                        .appName("Population Application")
                        .master("local[4]")
                        .getOrCreate()
            val sc = spark.sparkContext
            sc.setLogLevel("ERROR")
      
            val filerdd = sc.textFile("src/main/scala/World_Population_Per_Country.csv")
            
            
            //key-value rdd (country , list(y1990,y2000,...))
            val kvrdd:RDD[(wordpopulation)]=filerdd.map({row =>
                        val fields = row.split(",")
                        if (fields.length==17) wordpopulation (fields(3).toString,List(fields(5).toLong,fields(6).toLong,fields(7).toLong,fields(8).toLong,fields(9).toLong,fields(10).toLong,fields(11).toLong,fields(12).toLong,fields(13).toLong,fields(14).toLong,fields(15).toLong,fields(16).toLong))
                        else wordpopulation("None",List[Long]() )
                  }).filter(x => x.countryName!="None").cache()
           
            val mostPopulouscountry:String=kvrdd.max()(new Ordering[wordpopulation]() {
                         override def compare(x: wordpopulation, y: wordpopulation): Int = 
                           Ordering[Long].compare(x.populationPeryear.last, y.populationPeryear.last)
                  }).countryName
                              
             
            val lessPopulouscountry:String=kvrdd.min()(new Ordering[wordpopulation]() {
                         override def compare(x: wordpopulation, y: wordpopulation): Int = 
                           Ordering[Long].compare(x.populationPeryear.last, y.populationPeryear.last)
                  }).countryName
           
            val mostPopulouscountryIn2000:Array[String]=kvrdd.sortBy(x => x.populationPeryear(1),false).map(x => x.countryName).take(3)
           
            val decreasedPopulation:Array[String]=kvrdd.sortBy(x => x.populationPeryear.last-x.populationPeryear.head,false).map(x => x.countryName).collect()
            
            val biggestIncrease:String=kvrdd.filter(x => x.populationPeryear(9)-x.populationPeryear(1)>0 ).max()(new Ordering[wordpopulation]() {
                         override def compare(x: wordpopulation, y: wordpopulation): Int = 
                           Ordering[Long].compare(x.populationPeryear(9)-x.populationPeryear(1), y.populationPeryear(9)-y.populationPeryear(1))
                  }).countryName
             
            val lowestDecrease:String=kvrdd.filter(x =>x.populationPeryear(2)-x.populationPeryear(0)<0).min()(new Ordering[wordpopulation]() {
                         override def compare(x: wordpopulation, y: wordpopulation): Int = 
                           Ordering[Long].compare(x.populationPeryear(2)-x.populationPeryear(0), y.populationPeryear(2)-y.populationPeryear(0))
                  }).countryName

            println("""       
                              |--------------------------------------------------------------------------- |
                              | What Do you search :                                                       |
                              | The most populus country        [1]                                        |
                              | The less populus country       [2]                                        |
                              | The most 3 populus countries in 2000  [3]                                  |
                              | The countries that have known population decline   [4]                     | 
                              | The country that have known the biggest increase between 2000 and 2015 [5] |         
                              | The country that have known the lowest decrease between 1990 and 2008  [6] |
                              |                                                                            |
                              | Type 0 to exit                                                             |
                              |----------------------------------------------------------------------------|""")
            breakable {
                  while(true ){
                        println("\n" +
                              " Enter your choice :")
                        val inputInt : Int = readInt()
            
                        inputInt match {
                         case 1 => println(s"""-----------------------------------------------
                       | The most populus country is ==>   ${mostPopulouscountry}
                       |----------------------------------------------""".stripMargin)
                        case 2 => println(s"""-----------------------------------------------
                        |The less populus country is ==>   ${lessPopulouscountry}
                        |----------------------------------------------""".stripMargin)
                        case 3 => println(s"""-----------------------------------------------
                        | The most 3 populus countries in 2000 are ==>   
                        |${mostPopulouscountryIn2000.mkString("\n")}
                        |----------------------------------------------""".stripMargin)
                        case 4 =>println(s"""-----------------------------------------------
                       | The countries that have known population decline are ==>   
                       |${decreasedPopulation.mkString("\n")}
                       |----------------------------------------------""".stripMargin)
                        case 5 =>println(s"""-----------------------------------------------
                       | The country that have known the biggest increase between 2000 and 2015 is ==>   
                       |${biggestIncrease}
                       |----------------------------------------------""".stripMargin)
                        case 6 =>println(s"""-----------------------------------------------
                        | The country that have known the lowest decrease between 1990 and 2008 is ==>   
                        |${lowestDecrease}
                        |----------------------------------------------""".stripMargin)
                        
                        case 0 => break
                        case _ =>  println("Unknown option §") 
                  
                  }
            }
            }
      }
}