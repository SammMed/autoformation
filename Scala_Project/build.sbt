ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.11.12"

lazy val root = (project in file("."))
  .settings(
    name := "Scala_Project"
  )
libraryDependencies += "org.scala-lang.modules" %% "scala-xml" % "1.2.0"