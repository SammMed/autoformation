import scala.collection.mutable.ListBuffer
import scala.xml._
import scala.io.Source

class Person(var name: String, var age: Int){
  def getName() : String = {

    return name;
  }


  def getAge() : Int = {

    return age;
  }
  override def toString() : String = {

    return "[name: " + name +
      ",age = " + age+"]";
  }

}
object Main extends App {
  val xmldata = XML.loadFile("src/main/scala/liste_noms_age.xml")
  //println(xmldata)
  var list_person = new ListBuffer[Person]()
  val personNodes = (xmldata \\ "person")
  personNodes.foreach{ n =>
    val name  = (n \ "name").text
    val age = (n \ "age").text.toInt
    val p=new Person(name,age)
    list_person += p


  }
  //list_person.foreach(p=>println(p.toString()))
  println("Le plus jeune: " + list_person.minBy(_.getAge()))
  println("Le plus vieux: " + list_person.maxBy(_.getAge()))
  println("Le nom le plus long : " + list_person.maxBy(_.getName().length))
  println("Le nom le plus court : " + list_person.minBy(_.getName().length))
  println("la moyenne d'age: "+ list_person.map(_.getAge()).sum / list_person.length)
}
