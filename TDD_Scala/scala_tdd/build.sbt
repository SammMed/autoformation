name := "tdd_scala"
version := "1.0"
scalaVersion := "2.13.8"

libraryDependencies ++= Seq(
                        "org.scalactic" %% "scalactic" % "3.2.11",
                        "org.scalatest" %% "scalatest" % "3.2.11" % "test"
                           )

resolvers += "Artima Maven Repository" at "https://repo.artima.com/releases"