import org.scalatest.flatspec.AnyFlatSpec

class StringCalulTest extends AnyFlatSpec {
"empty string" should "return 0" in {
    assert(StringCalculator.add("") == 0)
}
"1,2 string" should "return 3" in {
    assert(StringCalculator.add("1,2") == 3)
}

s"newline separator 1,2\\n3" should "return 6" in {
     assert(StringCalculator.add("1,2\n3") == 6)
}
  
}
