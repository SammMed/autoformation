import os
import sqlalchemy as db

# extract cities data and store them in a dictionnary
def getCities():
    files = os.listdir("cities_inhabitants")
    cities=[]
    for file in files :
        path="cities_inhabitants/"+file
        with open(path, "r") as f:
             habitants =int( f.read())
             cities.append({'city':file.split(".")[0].lower(),'population':habitants})
    return cities 

# specify database configurations
config = {
    'host': 'localhost',
    'port': 3306,
    'user': 'root',
    'password': 'root_pass',
    'database': 'db'}
db_user = config.get('user')
db_pwd = config.get('password')
db_host = config.get('host')
db_port = config.get('port')
db_name = config.get('database')


# specify connection string
connection_str = f'mysql+pymysql://{db_user}:{db_pwd}@{db_host}:{db_port}/{db_name}'
# connect to database
engine = db.create_engine(connection_str)
connection = engine.connect()# pull metadata of a table
metadata = db.MetaData(bind=engine)
table = db.Table('test_table', metadata, autoload=True, autoload_with=engine)

#inserting data

#query = db.insert(table) 
#ResultProxy = connection.execute(query,getCities())

#show all records
query = db.select([table]) 
ResultProxy = connection.execute(query)
ResultSet = ResultProxy.fetchall()
print(ResultSet)



