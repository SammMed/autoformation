from flask import Flask, request, flash, url_for, redirect, render_template
import mysql.connector
from pyrsistent import v

app = Flask(__name__)

mydb = mysql.connector.connect( host="localhost",user="root",password="root_pass",database="db")

@app.route('/', methods = ['GET', 'POST'])
def search():
   if request.method == 'GET':
       return render_template('search.html')
   if request.method == 'POST':
             if not request.form['name'] :
                 msg='Please enter a city name'
             else:
                 city=request.form['name'].lower()
                 mycursor = mydb.cursor()
                 mycursor.execute("SELECT population FROM test_table  WHERE city = %s",(city,))
                 myresult = mycursor.fetchone()
                 if myresult :
                     msg='The population in '+city+' is '+str(myresult[0])
                 else:
                     msg='City not found' 

             return render_template('result.html',msg=msg)
@app.route('/add', methods = ['GET','POST'])
def add():
    if request.method=='GET':
        return render_template('new.html')
    if request.method == 'POST':
             if not request.form['name'] or not request.form['population'] :
                 msg='city name or population is missing'
             else :
                 city=request.form['name'].lower()
                 pop=int(request.form['population'])
                 mycursor = mydb.cursor()
                 mycursor.execute( "INSERT INTO test_table (city, population) VALUES (%s, %s)",(city,pop,))
                 mydb.commit()
                 msg= 'record inserted successfully' 
             return render_template('result.html',msg=msg)





       

    
if __name__ == '__main__':
     app.run(debug = True)