import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

public class InterstingInteger {
    public static void main (String[] args) {

        Scanner in = new Scanner(new BufferedReader(new InputStreamReader(System.in)));
        int T = in.nextInt();
        for (int t = 0; t < T; t++) {
            long a = in.nextLong();
            long b = in.nextLong();
            if (a == 0) a = 1;
            if  (b==0) System.out.println("Case #"+(t+1)+": 0");
            numOfInteresting(a,b,t);

        }
    }

    private static void numOfInteresting(long a, long b,int t) {
        int res =0;
        for (long x = a;x <= b;x++){
            int sum = String.valueOf(x).chars().map(Character::getNumericValue).sum();
            int product = String.valueOf(x).chars().map(Character::getNumericValue).reduce(1,(e,d)-> e*d);
            if (product % sum == 0) res++;
        }
        System.out.println("Case #"+(t+1)+": "+(res));

    }
}
