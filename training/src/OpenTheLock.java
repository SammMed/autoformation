import java.util.*;
import java.util.stream.Collectors;

public class OpenTheLock {
    public static  int openLock(String[] deadends, String target) {
        Set<String> visited = new HashSet<>();
        List<String> deadens =Arrays.asList(deadends);
        Queue<Map<String,Integer>> q = new LinkedList<>();
        q.add(Map.of("0000",0));
        if (deadens.contains("0000")) return -1;
        while (!q.isEmpty()){
            Map<String, Integer> e = q.remove();
            String node =e.keySet().iterator().next();
            int turns = e.values().stream().findFirst().get();
            if (node.equalsIgnoreCase(target)) return turns;
            for (String child : combinate(node)){
                if (!visited.contains(child) && !deadens.contains(child)){
                    visited.add(child);
                    q.add(Map.of(child,turns+1));
                }
            }
        }

        return -1;
    }
    public static List<String> combinate(String s){
        List<String> res = new ArrayList<>();
        for(int i=0;i<4;i++){
            StringBuilder temp =new StringBuilder(s);
                 if(s.charAt(i)=='0')
                 { temp.setCharAt(i,'1');
                        res.add(temp.toString());
                        temp.setCharAt(i,'9');
                        res.add(temp.toString());
                 }
                else if(s.charAt(i)=='9')
                { temp.setCharAt(i,'0');
                    res.add(temp.toString());
                    temp.setCharAt(i,'8');
                    res.add(temp.toString());
                }
                else{ temp.setCharAt(i,(char)(Character.getNumericValue(s.charAt(i))+1+'0'));
                    res.add(temp.toString());
                    temp.setCharAt(i,(char)(Character.getNumericValue(s.charAt(i))-1+'0'));
                    res.add(temp.toString());
                }

        }
        return res;
    }
    public static void main (String[] args){
        String[] deadens ={"0201","0101","0102","1212","2002"};
        String target ="0202";
        System.out.println(openLock(deadens,target));

    }
}
