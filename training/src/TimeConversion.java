public class TimeConversion {

    public static void timeConversion(String s){
        String result;
        int hour= Integer.parseInt(s.split(":")[0]);
        String pmOrAm=s.split(":")[2].substring(2,4);
        if (pmOrAm.equalsIgnoreCase("AM") && hour<12){
            result=s.replace("AM","");
        }
        else if (pmOrAm.equalsIgnoreCase("AM")&& hour==12){
            result="00"+s.substring(2).replace("AM","");
        }
        else if (pmOrAm.equalsIgnoreCase("PM") && hour ==12){
            result=s.replace("PM","");
        }
        else {
           result= hour + 12 +s.substring(2).replace("PM","");
        }
        System.out.println(result);
    }
    public static void main(String[] args){
        timeConversion("12:01:00AM");
        timeConversion("12:01:00PM");
    }
}
