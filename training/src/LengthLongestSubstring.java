import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class LengthLongestSubstring {
    /* Sliding window
       [a,b,c,b,d,a]   visted [1,1,0,0,0....]
        i j                   'a','b','c'.....
        [a,b,c,b,d,a]   visted [1,1,1,0,0....]
        i    j                'a','b','c'.....
        [a,b,c,b,d,a]   visted [1,1,1,0,0....]
        i      j              'a','b','c'.....
        visited['b']== true advance left cursor i until remove 'b' occurence
        [a,b,c,b,d,a] updated visited [0,0,1,1,0....]
             i j
    */
    public static int lengthOfLongestSubstring(String s) {
        int left = 0;
        int right = 0;
        boolean[] visited = new boolean[26];
        int res = 0;
        while (right < s.length()){
            while (visited[s.charAt(right)-'a']){
                visited[s.charAt(left)-'a']=false;
                left++;
            }

            visited[s.charAt(right)-'a'] = true;
            res = Math.max(res,right-left+1);
            right++;

        }
        return res;
    }
    public static int lengthOfLongestSubstringNativeApproach(String s) {
        int res = 0;
        for ( int i = 0; i < s.length() ; i++){
            boolean[] visited = new boolean[26];
            for ( int j = i; j < s.length() ; j++){
                if ( visited[s.charAt(j)-'a'])
                    break;
                visited[s.charAt(j)-'a']=true;
                res = Math.max(res,j-i+1);
            }
        }
        return res;
    }
    public static void main(String[] args) {
        System.out.print(lengthOfLongestSubstringNativeApproach("dvdf"));
    }

}
