import java.util.Arrays;
import java.util.List;

public class CompareWithSpecificOrder {

    public static boolean compareTwoWords(String word1,String word2,String order){
        int i=0,j=0;
        while ( i<word1.length() && j < word2.length())
        {
            int i1 = order.indexOf(word1.charAt(i));
            int i2 = order.indexOf(word2.charAt(j));
            if (i1 < i2)
                return true;
            else if (i1 > i2)
                return false;
            i++;
            j++;
        }

        return word1.length() <= word2.length();
    }
    public static boolean areSorted(List<String> words,String order){

        int i=0;
        while (i<words.size()-1){
            if (compareTwoWords(words.get(i), words.get(i + 1),order))
                i++;
            else
                return false;
        }
        return i==words.size()-1;
    }
    public static void main (String[] args){
        List<String> words = Arrays.asList("word", "world","row");
        String order = "worldabcefghijkmnpqstuvxyz";
        System.out.println(areSorted(words,order));
    }
}
