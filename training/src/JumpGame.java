import java.util.Arrays;

public class JumpGame {
    public static boolean canJump(int[] nums) {
        return backtracking(0,nums);
    }
    public static boolean backtracking (int i, int[] nums){

        if(i== nums.length-1) return true;
        if (nums[i]==0 && i<nums.length-1) return false;
        int maxJump = Math.min(i+nums[i],nums.length-1);
        for(int j=i+1;j<=maxJump;j++){
            if (backtracking(j,nums)) return true;
        }

        return false;
    }
    public static void main (String[] args){
        int[] nums ={2,3,1,1,4};
        System.out.println(canJump(nums));
    }
}
