import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Anagram {
    public static boolean isAnagram(String s, String t) {
        Map<Character, Integer> map = new HashMap<>();
        if (s.length() != t.length())
                return false;
        for (Character ch : s.toCharArray()){
            map.put(ch, map.get(ch) == null ? 1 : map.get(ch) + 1);
        }
        for (Character ch: t.toCharArray()){
            if (map.get(ch)==null || map.get(ch)==0)
                return false;

            map.put(ch, map.get(ch) - 1);
        }
        return true;
    }
    public static void main (String[] args){

        System.out.println(isAnagram("nagaram","anagramm"));
    }
}
