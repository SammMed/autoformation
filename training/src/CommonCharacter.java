import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CommonCharacter {
    public static List<String> commonChars(String[] words) {
        Map<Character,Integer> map = new HashMap<>();
        List<String> result  = new ArrayList<>();

        for (int i=0;i<words.length; i++){
            for(char ch : words[i].toCharArray()){
                if (map.get(ch) == null){
                    for (int j=0;j<words.length; j++){
                        int freq = (int) words[j].chars().filter(c -> c == ch).count();
                        map.put(ch,map.get(ch)==null?freq:Math.min(map.get(ch),freq));
                    }
                }
            }
        }




        for (Map.Entry<Character, Integer> entry : map.entrySet()){
                result.addAll(Collections.nCopies(entry.getValue(), String.valueOf(entry.getKey())));
        }

        return result;
    }
    public static void main (String[] args){
        String [] words = {"coool","lck","rler"};
        System.out.println(commonChars(words));
    }
}
