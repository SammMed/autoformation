import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PalindromeFreeStrings {
    public static void main (String[] args){

        Scanner in = new Scanner(new BufferedReader(new InputStreamReader(System.in)));
        int T = in.nextInt();
        for(int t=0;t<T;t++){
            int len = in.nextInt();
            String str= in.next();
            List<String> l = new ArrayList<>();
            combinate(str,l);
            boolean capturer= false;
             for (String s : l){
                 boolean flag =false;
                    if(s.length()>=5){
                        int i=1;
                        while (i<s.length() && !flag){
                            for( int j=0;j<s.length()-i;j++){
                                if (isPalindrome(s.substring(j,s.length()-i+1))>= 5){
                                    flag=true;
                                    break;
                                }

                            }
                            i++;

                        }
                    }
                if(!flag) {
                    System.out.println("Case #"+(t+1)+": POSSIBLE");
                    capturer=true;
                    break;
                }



             }
             if (!capturer)
             {System.out.println("Case #"+(t+1)+": IMPOSSIBLE");}
        }
    }
    public static List<String> combinate(String s,List<String> res){

        if (s.contains("?"))
        {   combinate(replace(s,"?","0"),res);
            combinate(replace(s,"?","1"),res);
        }else{
            res.add(s);

        }
        return res;
    }
    public static String replace(String s , String c , String c1){
        return s.replaceFirst(String.format("[%s]",c),c1);

    }
    public static int isPalindrome(String s){
        int i = 0;
        int j = s.length()-1;
        while (s.charAt(i)==s.charAt(j) && i<j) {
            i++;
            j--;

        }
        if((s.charAt(i)==s.charAt(j)))
        { return s.length()  ;}
        return 0;
    }
}
