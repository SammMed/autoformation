import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;

public class Ratios {
    public static void plusMinus(List<Integer> arr) {
        // Write your code here
        Map<String,Integer> ratioMap = new HashMap();
        arr.stream().map(x ->{
           if (x>0) ratioMap.put("pos",ratioMap.get("pos")!=null?ratioMap.get("pos")+1:1);
           else if (x<0) ratioMap.put("neg",ratioMap.get("neg")!=null?ratioMap.get("neg")+1:1);
           else ratioMap.put("zero",ratioMap.get("zero")!=null?ratioMap.get("zero")+1:1);

           return ratioMap;
        }).collect(Collectors.toList()).get(0);
        if (!ratioMap.containsKey("pos")) ratioMap.put("pos",0);
        if (!ratioMap.containsKey("neg")) ratioMap.put("neg",0);
        if (!ratioMap.containsKey("zero")) ratioMap.put("zero",0);
        String result = String.format("%.6f\n%.6f\n%.6f", (float)ratioMap.get("pos") / arr.size(), (float)ratioMap.get("neg") / arr.size()
                , (float)ratioMap.get("zero") / arr.size());
        System.out.println(result);
    }

    public static void main (String[] args){
        Scanner in = new Scanner(new BufferedReader(new InputStreamReader(System.in)));
        List<Integer> arr=new ArrayList<>();
        int n = in.nextInt();
        for (int i=0; i<n;i++){
            arr.add(in.nextInt());
        }
        plusMinus(arr);


    }
}
