import java.util.*;

public class Kclosest {
    public int[][] kClosest(int[][] points, int k) {
        Arrays.sort(points,Comparator.comparingInt((int[] x) -> (x[0] * x[0] + x[1] * x[1])));
        return Arrays.copyOfRange(points, 0, k);
    }
}
