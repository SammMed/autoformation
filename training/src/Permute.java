import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.time.LocalDate;

public class Permute {

    public static void backtrack(List<List<Integer>> list, List<Integer> tempList, int [] nums){
        if(tempList.size() == nums.length){
            list.add(new ArrayList<>(tempList));
        } else{
            for (int num : nums) {
                if (tempList.contains(num)) continue; // element already exists, skip
                tempList.add(num);
                backtrack(list, tempList, nums);
                tempList.remove(tempList.size()-1);
            }
        }
    }
    public static void addDays(int d,int m,int y,int d0){
        if (d0==0)
            System.out.printf("%d/0%d/%d%n",d,m,y);
        HashMap<Integer, Integer> map = new HashMap<>();
        for (int i=1;i<13;i++) {
            switch (i) {
                case 2:
                    map.put(i, 28);
                    break;

                case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                case 12:
                    map.put(i, 31);
                    break;

                case 4:
                case 6:
                case 9:
                case 11:
                    map.put(i, 30);
                    break;

            }
        }
        int nbOfDays = map.get(m);
        if ( d+d0 <= nbOfDays)
            System.out.printf("%d/0%d/%d%n",d+d0,m,y);
        else{
            if (m+1>12)
                addDays(0,m+1-12,y+1,d0-nbOfDays+d);
            else
                addDays(0,m+1,y,d0-nbOfDays+d);
        }

    }

    public static void main (String[] args) {
       /* List<List<Integer>> list = new ArrayList<>();
        int [] nums = {1,2,3};
        backtrack(list, new ArrayList<>(), nums);
        System.out.println(list);*/
        addDays(19,8,2024,530);
    }
}
