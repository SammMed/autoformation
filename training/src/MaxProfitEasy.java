import java.util.*;


public class MaxProfitEasy {
    public static int maxProfit(int[] prices) {
        int maxProfit = 0;
        for (int i = 0; i < prices.length-1; i++) {
            int diff = prices[i + 1] - prices[i];
            if (diff > 0) maxProfit = maxProfit + diff;
        }
        return maxProfit;
    }
    public static void main (String[] args){
        int[] prices ={1,2,3,4,5};
        System.out.println(maxProfit(prices));
    }
}
