import java.util.*;

public class PrisonAfterNdays {

        public static int[] prisonAfterNDays(int[] cells, int n) {
            List<int[]> comb = new ArrayList<>();
            return PrisonRec(cells, n%14==0?14:n%14, comb);

        }

        private static int[] PrisonRec(int[] cells, int n, List<int[]> comb) {
            int [] temp = new int[cells.length];
            /*
            if (comb.stream().anyMatch(a -> Arrays.equals(a, cells)))
                System.out.printf("cycle detected at day %d",n);
             */
            if (n == 0)
                return cells;
            for (int i = 0; i< cells.length; i++){
                if(i==0 || i== cells.length-1)
                    temp[i]=0;
                else if ((cells[i-1]==0 && cells[i+1]==0) || (cells[i-1]==1 && cells[i+1]==1))
                    temp[i]=1;
                else
                    temp[i]=0;
            }
            //comb.add(cells);
            return PrisonRec(temp, n - 1,comb);
        }
    public static void main(String[] args) {
        int [] cells = {1,0,0,1,0,0,1,0};

            prisonAfterNDays(cells,15);
    }
}

