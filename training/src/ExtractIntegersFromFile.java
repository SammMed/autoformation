import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ExtractIntegersFromFile {
    public static int[] extractArray() {
        String filePath = "C:\\Users\\MohamedSAMMARI\\autoFormation\\autoformation\\training\\src\\testCase.txt"; // Replace with your file path

        try {
            // Read the file content
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line = reader.readLine();
            reader.close();

            if (line != null) {
                // Split the line by commas
                String[] stringValues = line.split(",");

                // Convert the string values to integers
                List<Integer> integerList = new ArrayList<>();
                for (String value : stringValues) {
                    integerList.add(Integer.parseInt(value.trim()));
                }

                // Convert the list to an int array
                int[] intArray = new int[integerList.size()];
                for (int i = 0; i < integerList.size(); i++) {
                    intArray[i] = integerList.get(i);
                }

                // Print the array to verify

                return intArray;

            } else {
                System.out.println("The file is empty.");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            System.out.println("The file contains non-integer values.");
            e.printStackTrace();
        }
        return new int[1];
    }
}