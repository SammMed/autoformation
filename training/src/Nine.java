import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;

public class Nine {
    public static void main (String[] args) {

        Scanner in = new Scanner(new BufferedReader(new InputStreamReader(System.in)));
        int T = in.nextInt();
        in.nextLine();
        for (int t = 0; t < T; t++) {
            String n =in.nextLine();
            int[] numbers = n.chars().map(Character::getNumericValue).toArray();
            List<Integer> l =new ArrayList<>();
            for (int a:numbers){l.add(a);}
            int sum = n.chars().map(Character::getNumericValue).sum();
            int dig= findDigit(sum);
            if (dig == 0) l.add(1,0);
            else{
                int i=0;
                while (i<l.size() && dig>=l.get(i)) i++;
                l.add(i,dig);
            }

            System.out.println("Case #"+(t+1)+": "+(l.stream().map(String::valueOf)
                    .collect(Collectors.joining(""))));

        }
    }
    public static int findDigit (int sum){
        if (sum ==9) return 0;
        else if (sum>9) return findDigit(sum -9);
        else return 9 -sum;
    }
}
