import java.util.*;

public class KthFactor {
    public static int kthFactor(int n, int k) {
        List<Integer> factor_asc = new ArrayList<>();
        List<Integer> factor_desc = new ArrayList<>();
        for (int i = 1 ; i * i <= n ; i++){
            if ( n % i  == 0 ){
                factor_asc.add(i);
                if ( n / i != i )
                    factor_desc.add(n/i);
            }
        }
        if ( k <= factor_asc.size())
            return factor_asc.get(k-1);
        k-= factor_asc.size();
        if ( k <= factor_desc.size())
            return factor_desc.get(factor_desc.size()-k);

        return -1;
    }
    public static void main(String[] args) {
        System.out.print(kthFactor(7,2));
    }
}
