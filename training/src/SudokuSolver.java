import java.util.ArrayList;
import java.util.List;

public class SudokuSolver {

    private static boolean solveCell (int row ,int col ,char[][] board){
        if (row == 9)
            return true;
        if(col == 9)
            return solveCell(row+1,0,board);
        if(board[row][col]!='.')
            return solveCell(row,col+1,board);

        for (int i=1 ; i<10 ; i++){
            if (isValidPlace(row,col,board,(char)(i+48))){
                board[row][col]=(char)(i+48);
                if (solveCell(row,col+1,board))
                    return true;

            }
            board[row][col]='.';
        }
        return false;
    }

    private static boolean isValidPlace(int row, int col, char[][] board,char choice) {
        for (int i=0; i<9; i++){
            if (board[row][i] == choice)
                return false;
            if (board[i][col] == choice)
                return false;

        }
        int rowStart = 3 * (row / 3) ;
        int colStart = 3 * (col / 3) ;
        for ( int i = rowStart ; i<rowStart+3;i++) {
            for (int j = colStart; j < colStart + 3; j++) {
                if (board[rowStart][colStart] == choice)
                    return false;
            }
        }
        return true;
    }
    public static void solveSudoku(char[][] board) {

        solveCell(0,0,board);
    }
    public static void main (String[] args) {
        char [][] board = {{'5', '3', '.', '.', '7', '.', '.', '.', '.'},
                {'6', '.', '.', '1', '9', '5', '.', '.', '.'},
                {'.', '9', '8', '.', '.', '.', '.', '6', '.'},
                {'8', '.', '.', '.', '6', '.', '.', '.', '3'},
                {'4', '.', '.', '8', '.', '3', '.', '.', '1'},
                {'7', '.', '.', '.', '2', '.', '.', '.', '6'},
                {'.', '6', '.', '.', '.', '.', '2', '8', '.'},
                {'.', '.', '.', '4', '1', '9', '.', '.', '5'},
                {'.', '.', '.', '.', '8', '.', '.', '7', '9'}
        };
        solveSudoku(board);
        for (int i = 0 ;i<9 ; i++){
           for ( int j=0 ; j<9 ; j++){
               if (j == 8){
                   System.out.print(board[i][j]);
                   System.out.print('\n');
               }else{
                System.out.print(board[i][j]+",");}
           }
        }

    }
}
