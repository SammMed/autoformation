import java.util.*;

public class TwoSum {
    public int[] twoSum(int[] nums, int target) {
        int[] res = new int[2];
        for(int i=0;i<nums.length;i++){
            for(int j=0;j<nums.length && j!=i;j++){
                if (nums[i]+nums[j] == target){
                    res[0]=i;
                    res[1]=j;
                    return res;
                }
            }
        }
        return res;
    }
    public int[] twoSum2(int[] nums, int target) {
        Map<Integer, List<Integer>> map = new HashMap<>();
        int[] res = new int[2];
        for(int i=0;i<nums.length;i++){
            int key = target - nums[i];
            List<Integer> val = new ArrayList<>(List.of(i));
            map.put(key,val);
        }
        for(int i=0;i<nums.length;i++){
            if(map.get(nums[i]) != null) {
                map.get(nums[i]).add(i);
                Integer first = map.get(nums[i]).get(0);
                Integer second = map.get(nums[i]).get(1);
                if (first != second){
                    res[0] =first;
                    res[1] = second;
                    return res;
                }
            }
        }
        return res;
    }
}
