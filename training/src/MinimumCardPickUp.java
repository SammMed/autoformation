public class MinimumCardPickUp {
    public static int minimumCardPickup(int[] cards) {
        int res = cards.length+1;
        int[] visited = new int[1000000];
        for ( int i = 0; i < cards.length ; i++){
            if ( visited[cards[i]] > 0){
                res = Math.min(res,i-visited[cards[i]]+2);
            }
            visited[cards[i]]=i+1;
        }
        return res == cards.length + 1? -1 : res;
    }
    public static void main (String[] args) {
        int[] cards =ExtractIntegersFromFile.extractArray();
        long startTime = System.currentTimeMillis();
        int res = minimumCardPickup(cards);
        long endTime = System.currentTimeMillis();

        long duration = (endTime - startTime);
        System.out.print("Result: "+ res+" Execution Time "+duration);


    }
}
