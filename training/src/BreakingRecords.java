import java.util.Arrays;
import java.util.List;

public class BreakingRecords {

    public static List<Integer> breakingRecords(List<Integer> scores) {
        if (scores.size()==1) return Arrays.asList(0,0);
        int min=scores.get(0),max=scores.get(0);
        int mostRecord=0,leastRecord=0;
        for(int i=1;i<scores.size();i++){
            if (scores.get(i)>max) {
                max=scores.get(i);
                mostRecord+=1;
            }
            if (scores.get(i)<min) {
                min=scores.get(i);
                leastRecord+=1;
            }
        }
        return Arrays.asList(mostRecord,leastRecord);
    }
    public static void main(String[] args) {
        System.out.println(breakingRecords(Arrays.asList(12,24,10,24)));
    }
}
