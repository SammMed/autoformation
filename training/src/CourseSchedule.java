import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class CourseSchedule {
    public static boolean canFinish(int numCourses, int[][] prerequisites) {
        List<List<Integer>> adj = new ArrayList<>();
        // Stores in-degree of each vertex
        int[] inDegree = new int[numCourses];

        // Queue to store vertices with 0 in-degree
        Queue<Integer> q = new LinkedList<>();
        int visited = 0; //

        // Initialize adjacency list
        for (int i = 0; i < numCourses; i++) {
            adj.add(new ArrayList<>());
        }
        for (int i = 0; i < numCourses; i++){
            for (int j = 0; j < prerequisites.length; j++){
                if (prerequisites[j][1] == i)
                    adj.get(i).add(prerequisites[j][0]);
            }
        }
        // Calculate in-degree of each vertex
        for (int u = 0; u < numCourses; u++) {
            for (int v : adj.get(u)) {
                inDegree[v]++;
            }
        }

        // Enqueue vertices with 0 in-degree
        for (int u = 0; u < numCourses; u++) {
            if (inDegree[u] == 0) {
                q.offer(u);
            }
        }

        // BFS traversal
        while (!q.isEmpty()) {
            int u = q.poll();
            visited++;

            // Reduce in-degree of adjacent vertices
            for (int v : adj.get(u)) {
                inDegree[v]--;
                // If in-degree becomes 0, enqueue it
                if (inDegree[v] == 0) {
                    q.offer(v);
                }
            }
        }

        // If not all vertices are visited, cycle
        return visited == numCourses;
    }
    public static void main (String[] args){
    int [][] prerequisites = {{1,0},{0,1}};
    System.out.print(canFinish(2,prerequisites));
    }
}
