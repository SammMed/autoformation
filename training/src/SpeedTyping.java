import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;

public class SpeedTyping {
    public static void main (String[] args) {

        Scanner in = new Scanner(new BufferedReader(new InputStreamReader(System.in)));
        int T = in.nextInt();
        for(int t=0;t<T;t++){
            String strToType = in.next();
            char[] strTyped = in.next().toCharArray();
            int i=0;
            for (char c : strTyped){

                if (i<strToType.length() && c == strToType.charAt(i)){
                    i++;
                    continue;
                }
            }
            if (i==strToType.length()){
                System.out.println("Case #"+(t+1)+": "+(strTyped.length-i));
            }
            else System.out.println("Case #"+(t+1)+": IMPOSSIBLE");
        }
    }
}
