import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

class TargetSomme {
    public static int findTargetSumWays(int[] nums, int target) {
        int[][] dp = new int[nums.length+1][2001];
        for (int[] row: dp) {
            Arrays.fill(row, Integer.MIN_VALUE);
        }

    return combinate(0,nums,0,target,dp);

    }
    public static int combinate (int start,int [] nums,int ind,int target,int dp[][]){
        if (dp[ind][start+1000] != Integer.MIN_VALUE) return dp[ind][start+1000];
        if (ind<nums.length){
            int res =combinate(start+nums[ind],nums,ind+1,target,dp)+
            combinate(start-nums[ind],nums,ind+1,target,dp);
            dp[ind][start+1000]=res;
            return res;

        }
        else{
            return start==target?1:0;
        }
    }
    public static void main (String[] args){
        int[] nums ={6,20,22,38,11,15,22,30,0,17,34,29,7,42,46,49,30,7,14,5};
        int res = 0 ;
        long start1 = System.currentTimeMillis();
        res=findTargetSumWays(nums,28);
        long end1 = System.currentTimeMillis();
        System.out.println("temps:"+(end1-start1) +" result = "+res);
    }

    }