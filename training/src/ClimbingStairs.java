public class ClimbingStairs {

        public static int climbStairsdp(int n,int[] dp) {
                if (dp[n]!=0) return dp[n];

                if (n==0 || n ==1) {
                        dp[n]=1;
                        return 1;
                }

                int ans=climbStairsdp(n-1,dp)+climbStairsdp(n-2,dp);
                dp[n]=ans;
                return ans;

        }
        public static void main (String[] args){
                int[] dp =new int[46];
                long start1 = System.currentTimeMillis();
                int res=climbStairsdp(5,dp);
                long end1 = System.currentTimeMillis();
                System.out.println("Temps :"+(end1-start1)+" ms "+"resulta "+res);
        }

}
