import java.util.*;
import java.util.stream.Collectors;

public class MiniMax {
    public static void minMax(List<Integer> arr){
        Collections.sort(arr);
        int min = 0,max=0;
        for (int i=0;i<arr.size()-1;i++){
            if (i<arr.size()-1) min+= arr.get(i);
            if (i>0) max+=arr.get(i);

        }
        System.out.println(String.format("%d %d",min,max));
    }
    public static void main(String[] args){
        List<Integer> arr =new ArrayList<>( Arrays.asList(1,2,3,4,5));
        minMax(arr);
    }
}
