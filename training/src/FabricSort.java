import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class FabricSort {
    static class product {
        public String color;
        public int durability;
        public int id;
        public product(String c,int d,int id){
            this.color=c;
            this.durability=d;
            this.id=id;}
    }
    public static void main (String[] args) {

        Scanner in = new Scanner(new BufferedReader(new InputStreamReader(System.in)));
        int T = in.nextInt();
        for (int t = 0; t < T; t++) {
            int N = in.nextInt();
            in.nextLine();
            List<product> l = new ArrayList<>();
            for(int n=0;n<N;n++){
                String[] c = in.nextLine().split(" ");
                l.add(new product(c[0],Integer.parseInt(c[1]),Integer.parseInt(c[2])));
            }
            List<product> sortedByColor = l.stream()
                    .sorted((p1,p2) -> p1.color.compareTo(p2.color) == 0?Integer.compare(p1.id,p2.id):p1.color.compareTo(p2.color))
                    .collect(Collectors.toList());
            List<product> sortedByDurability = l.stream()
                    .sorted((p1,p2) -> p1.durability == p2.durability ?Integer.compare(p1.id,p2.id):Integer.compare(p1.durability,p2.durability))
                    .collect(Collectors.toList());
            int res=0;
            while (res< l.size() && sortedByColor.get(res).equals(sortedByDurability.get(res)) ) res++;

            System.out.println("Case #"+(t+1)+": "+(res));
        }
    }
}
