import java.util.Arrays;

public class NumberOfWays {
    public static int numberOfWays(int startPos, int endPos, int k) {
        long M = 1000000007;
        int[][] dp = new int[3001][1001];
        for (int[] row: dp) {
            Arrays.fill(row, Integer.MIN_VALUE);
        }
     return (int) (backtracking(startPos,endPos,k,dp)%M);

    }
    public static int backtracking(int start,int end , int k, int[][] dp){
        long M = 1000000007;
        if (dp[start+1000][k] != Integer.MIN_VALUE) return dp[start+1000][k];
        if(k == 0) return (start==end)?1:0;
        int ans=backtracking(start+1,end,k-1,dp)+backtracking(start-1,end,k-1,dp);
        dp[start+1000][k]= (int) (ans%M);
        return (int) (ans%M);
    }
    public static void main (String[] args){


        System.out.println(numberOfWays(989,1000,99));
    }
}
