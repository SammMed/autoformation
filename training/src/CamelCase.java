import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CamelCase {
    public static void main(String[] args) {
        Scanner in = new Scanner(new BufferedReader(new InputStreamReader(System.in)));

        while (in.hasNextLine()) {
            String[] parts = in.nextLine().split(";");
            boolean combine = parts[0].equalsIgnoreCase("C");
            boolean itsClass = parts[1].equalsIgnoreCase("C");
            boolean itsMethod = parts[1].equalsIgnoreCase("M");
            boolean itsVar = parts[1].equalsIgnoreCase("V");
            if(!combine) {
                if (itsMethod)
                    System.out.println(splitExpr(parts[2],1,0)
                            .replace("()",""));
                else System.out.println(splitExpr(parts[2],1,0));
            }else{
                if (itsClass)
                    System.out.println(Arrays.stream(parts[2].split(" "))
                        .map(x -> x.replace(x.charAt(0),Character.toUpperCase(x.charAt(0))))
                        .collect(Collectors.joining("")));
                else if (itsMethod)
                    System.out.println(combineExpr(parts[2])+"()");
                else if (itsVar)
                    System.out.println(combineExpr(parts[2]));
            }
        }
        in.close();
    }
    public static String splitExpr(String s,int i,int lastStart) {

        int start = i;
        if (i >= s.length()) {return ""; }
        while (i < s.length() && !Character.isUpperCase(s.charAt(i))) {
            i++;
        }

        return  s.substring(start - 1, i).toLowerCase()+" "+splitExpr(s, i + 1,start).toLowerCase();

    }
    public static String combineExpr(String s){
            String[]parts = s.split(" ");
           return parts[0]+IntStream.range(1,parts.length)
            .mapToObj(i -> parts[i].replace(parts[i].charAt(0),
                    Character.toUpperCase(parts[i].charAt(0)))
            ).collect(Collectors.joining(""));
    }

}
