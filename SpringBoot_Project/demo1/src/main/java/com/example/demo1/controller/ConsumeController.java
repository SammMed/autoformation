package com.example.demo1.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.util.JSONPObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.json.simple.JSONObject;

import java.util.Arrays;
import java.util.List;

@RestController
public class ConsumeController {
    @GetMapping("/AgeAVG")
    public float ageAvg() throws ParseException {
        String uri= "http://localhost:8080/persons/list";
        Long result = 0L ;
        RestTemplate restTemplate= new RestTemplate();
        String response = restTemplate.getForObject(uri,String.class);
        JSONParser parser =new JSONParser();
        JSONArray jsonresponse = (JSONArray) parser.parse(response);

        for (int i=0 ; i< jsonresponse.size();i++){

            JSONObject u = (JSONObject) jsonresponse.get(i);

           result +=(Long) u.get("age");
        }
        return (result/(jsonresponse.size()));

    }
}
