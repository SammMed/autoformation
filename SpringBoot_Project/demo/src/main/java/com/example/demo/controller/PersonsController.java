package com.example.demo.controller;

import com.example.demo.domain.entity.Person;
import com.example.demo.service.PersonService;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;


@RestController
@RequestMapping(value = "/persons")
public class PersonsController {
    @Autowired
    private PersonService personService;


   public PersonsController(PersonService ps){
       this.personService=ps;
   }


    @GetMapping("/list")
    public List<Person> getPersons(){
        return personService.getPersons();
    }

    @GetMapping("/{name}")
    public ResponseEntity<Long> getNameByAge(@PathVariable(value="name") String name){

        return personService.getAgeByName(name);
   }
}
