package com.example.demo.service;

import  com.example.demo.domain.entity.Person;
import  com.example.demo.domain.repository.PersonRepository;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import java.io.File;
import java.io.IOException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import java.io.FileReader;
import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class PersonService {
    private static final Logger logger = LoggerFactory.getLogger(PersonService.class);
    @Autowired
    private PersonRepository personRepository;

    @Scheduled(fixedRate = 180000)
    public void  generateData () throws IOException, ParseException {


        File jsonFile = new ClassPathResource("liste_noms_age.json").getFile();

        JSONParser parser =new JSONParser();

        JSONObject jsonObj = (JSONObject) parser.parse(new FileReader(jsonFile.getPath()));

        JSONArray users = (JSONArray) jsonObj.get("users");

        for ( Object o : users)
        {
            JSONObject u = (JSONObject) o;
            String name = (String) u.get("name");
            Long age =(Long) u.get("age");

            Person per = Person.builder()
                    .id(System.nanoTime())
                    .name(name)
                    .age(age)
                    .build();
            personRepository.save(per);


        }
        logger.info("Database populated every 3 MINS");
    }
    public List<Person> getPersons() {
        return personRepository.findAll();
    }

    public ResponseEntity<Long> getAgeByName(String name) {
        ResponseEntity<Long> response ;
        List<Person> pers = personRepository.getByName(name);

        if (!pers.isEmpty()){
            response = new ResponseEntity<>(pers.get(0).getAge(), HttpStatus.OK);
        }
        else{
            response = ResponseEntity.notFound().build();
        }

    return  response ;
    }
}
