package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class SquareController {

    @GetMapping("/home")
    public String home()
    {
        return "form";
    }
    @PostMapping("/result")
    @ResponseBody
    public String squareCal(@RequestParam("num") Integer arg) {
        System.out.println("argument "+ arg);
        Integer res= arg*arg;

        return "<p>"+arg.toString()+"² = "+res.toString()+"</p>";
    }
}
