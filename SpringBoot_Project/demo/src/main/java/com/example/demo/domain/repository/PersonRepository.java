package com.example.demo.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.example.demo.domain.entity.Person;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PersonRepository extends JpaRepository<Person,Long>{

    @Query("select u from person u where u.name = ?1")
    List<Person> getByName(String name);
}
